package practice.cl.com.kotlinmvp

import practice.cl.com.kotlinmvp.model.UserData

/**
 * Created by socomo on 11/13/17.
 */
interface MainPresenter {
    fun initialiseView()

    fun validateLoginData(email: String, password: String)

    fun loginResponseData(data: UserData)

}