package practice.cl.com.kotlinmvp

import android.content.Context
import practice.cl.com.kotlinmvp.model.UserData

class MainPresenterImpl(private val context: Context) : MainPresenter {


    private val view: MainView = context as MainView
    private val interactor: MainInteractor = MainInteractorImpl(context, this)


    override fun initialiseView() {
        view.init()
    }


    override fun validateLoginData(email: String, password: String) {
        if (email.length < 1) {
            view.setEmailError(context?.getString(R.string.error_msg_enter_email))
        } else if (!Validator.validateEmail(email)) {
            view.setEmailError(context?.getString(R.string.error_msg_invalid_email))
        } else if (password?.length < 1) {
            view.setPasswordError(context?.getString(R.string.error_msg_enter_password))
        } else if (!Validator.validatePassword(password)) {
            view.setPasswordError(context?.getString(R.string.error_msg_password_length))
        } else {
            interactor.loginUser(email, password)
        }
    }


    override fun loginResponseData(data: UserData) {
        if (data.data.isVerified) {
            //view.showToast("SUCCESS!! ${data.data.email}")
            view.showToast("${context.getString(R.string.success)} ${data.data.email}")
        } else {
            view.showToast(context.getString(R.string.failed_login))
        }
    }


}