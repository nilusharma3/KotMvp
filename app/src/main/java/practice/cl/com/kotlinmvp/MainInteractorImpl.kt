package practice.cl.com.kotlinmvp

import android.content.Context
import practice.cl.com.kotlinmvp.model.Data
import practice.cl.com.kotlinmvp.model.UserData

class MainInteractorImpl(val context: Context, val presenterInstance: MainPresenter) : MainInteractor {

    private val presenter: MainPresenter = presenterInstance
    override fun loginUser(email: String, password: String): Unit {
        var data: UserData = UserData()
        data.data = Data();
        data.data.email = email
        data.data.isVerified = password.equals("password")
        presenter.loginResponseData(data)
    }

}