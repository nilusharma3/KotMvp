package practice.cl.com.kotlinmvp

import android.util.Patterns

/**
 * Created by socomo on 11/13/17.
 */
class Validator {

    companion object {
        fun validateEmail(email: String): Boolean {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }


        fun validatePassword(password: String): Boolean {
            return password.length > 4
        }
    }


}