package practice.cl.com.kotlinmvp

import android.view.View

/**
 * Created by socomo on 11/13/17.
 */
interface MainView : View.OnClickListener {

    fun init()

    fun setEmailError(error: String)

    fun setPasswordError(error: String)

    fun showToast(string: String)

}