package practice.cl.com.kotlinmvp

import practice.cl.com.kotlinmvp.model.UserData

/**
 * Created by socomo on 11/13/17.
 */
interface MainInteractor {

    fun loginUser(email: String, password: String): Unit
}