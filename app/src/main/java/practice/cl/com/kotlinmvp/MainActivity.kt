package practice.cl.com.kotlinmvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView {

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenterImpl(this)
        presenter.initialiseView()
    }

    override fun init() {
        btnSignIn.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSignIn -> presenter.validateLoginData(etEmail.text.toString()
                    , etPassword.text.toString())

            else -> print("none")
        }
    }

    override fun setPasswordError(error: String) {
        etPassword.error = error
    }

    override fun setEmailError(error: String) {
        etEmail.error = error
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }


}
